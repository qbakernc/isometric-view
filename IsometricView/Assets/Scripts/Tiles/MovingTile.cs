﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTile : MonoBehaviour
{
    private Vector3Int cellPosition = new Vector3Int(0, 0, 0);
    private bool start;
    private  float frequency = .25f;
    private  float amplitude = 5;
    public float z = 4;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            float cosine = amplitude * Mathf.Cos(frequency * Time.time);
            transform.position = Utilities.CellToWorld(cosine, cellPosition.y, cellPosition.z); transform.position = Utilities.CellToWorld(cosine, cellPosition.y, cellPosition.z);
        }
    }
    
    public void Setup(bool start, Vector3Int cellPosition)
    {
        this.start = start;
        this.cellPosition = cellPosition;
    }
}
