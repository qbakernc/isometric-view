using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileControl : MonoBehaviour
{
    public Vector3Int cellPosition = new Vector3Int(0,0,0);
    public float z = 0;
    private bool clicked;
    private float timer;
    // Start is called before the first frame update
    void Awake()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color32(50, 131, 178, 255);
    }

    // Update is called once per frame
    void Update()
    {
        //gameObject.transform.position = Utilities.CellToWorld(cellPosition.x,cellPosition.y,z);
        if(clicked){
            gameObject.transform.position = Utilities.CellToWorld(cellPosition.x,cellPosition.y, -100 * Mathf.Sin(0.25f * timer) + z);
            timer += Time.deltaTime;
        }
    }

    public void Raise(){

    }
    void OnBecameInvisible(){
        Destroy(gameObject);
    }
    void OnMouseDown(){
        clicked = true;
    }
    void OnMouseEnter(){

        gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 131, 178, 255);
    }
    void OnMouseExit(){

        gameObject.GetComponent<SpriteRenderer>().color = new Color32(50, 131, 178, 255);
    }
}
