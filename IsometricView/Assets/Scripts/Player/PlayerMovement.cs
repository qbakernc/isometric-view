﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    Vector3Int cellPosition = new Vector3Int(0,0,2);
    public float scaleInput = 0.1f;
    private Vector2 scrollPosition;
    private Vector3 wmpOffset = Vector3.zero;
    private bool buttonPressed;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Horizontal")){
            if(Input.GetAxisRaw("Horizontal") == 1) {
                cellPosition = new Vector3Int(cellPosition.x, cellPosition.y - 1, cellPosition.z);
            } else if(Input.GetAxisRaw("Horizontal") == -1) {
                cellPosition = new Vector3Int(cellPosition.x, cellPosition.y + 1, cellPosition.z);
            }
            transform.position = Utilities.CellToWorld(cellPosition.x, cellPosition.y, cellPosition.z);
            buttonPressed = true;
        }
        if(Input.GetButtonDown("Vertical")){
            if(Input.GetAxisRaw("Vertical") == 1) {
                cellPosition = new Vector3Int(cellPosition.x + 1, cellPosition.y, cellPosition.z);
            } else if(Input.GetAxisRaw("Vertical") == -1) {
                cellPosition = new Vector3Int(cellPosition.x - 1, cellPosition.y, cellPosition.z);
            }
            transform.position = Utilities.CellToWorld(cellPosition.x, cellPosition.y, cellPosition.z);
            buttonPressed = true;
        }
    }
    private void FixedUpdate()
    {

    }
}
