﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class CameraControls : MonoBehaviour
{
    PixelPerfectCamera pixelPerfectCamera;
    private bool zoom;
    // Start is called before the first frame update
    void Start()
    {
        pixelPerfectCamera = GetComponent<PixelPerfectCamera>();
        //Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(1)){
            zoom = !zoom;
        }
        if(zoom){
            pixelPerfectCamera.assetsPPU = 68;
        } else {
            pixelPerfectCamera.assetsPPU = 32;
        }
    }
}
