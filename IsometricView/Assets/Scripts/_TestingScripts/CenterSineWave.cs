﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterSineWave : MonoBehaviour
{
    public Vector3Int cellPosition;
    private Vector3 worldPosition;
    private int childIndex;
    private float amplitude;
    private float freq = 4f;
    public float hShift;
    private bool start;
    // Start is called before the first frame update
    void Awake()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color32(50, 131, 178, 0);
        worldPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (start) {
            float wave = 0.25f * Mathf.Sin((freq * Time.time) - hShift) + (6f * Mathf.Cos((0.5f * Time.time)));
            float cosWave = 0.25f * Mathf.Cos((freq * Time.time) - hShift) + (2f * Mathf.Sin((0.5f * Time.time)));
            transform.position = new Vector3(cosWave + worldPosition.x, wave + worldPosition.y, transform.position.z);
        }
    }

    public void StartWave(bool start, Vector3Int cellPosition, int childIndex, float hShift)
    {
        this.childIndex = childIndex;
        this.cellPosition = cellPosition;
        this.hShift = hShift;
        gameObject.GetComponent<SpriteRenderer>().color = new Color32(0, 102, 180, 255);
        if(childIndex == 821)
            gameObject.GetComponent<SpriteRenderer>().color = new Color32(0, 102, 180, 1);
        this.start = start;
    }
}



