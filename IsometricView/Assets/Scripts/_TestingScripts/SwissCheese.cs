﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwissCheese : MonoBehaviour
{
    private Vector3Int cellPosition;
    private Vector3 worldPosition;
    private int childIndex;
    private float amplitude;
    private float freq;
    public float hShift;
    private bool start;
    // Start is called before the first frame update
    void Start()
    {
        worldPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (start) {
            float wave = .25f * Mathf.Sin((freq * Time.time) - hShift);
            transform.position = new Vector3(transform.position.x, wave + worldPosition.y, transform.position.z);
        }
    }

    public void StartWave(bool start, Vector3Int cellPosition, int childIndex)
    {
        this.childIndex = childIndex;
        this.cellPosition = cellPosition;
        hShift = Random.Range(0, 360) * Mathf.Rad2Deg;
        freq = Random.Range(0, 3f);
        this.start = start;
        gameObject.GetComponent<SpriteRenderer>().color = new Color32(50, 131, 178, 255);
    }
}
