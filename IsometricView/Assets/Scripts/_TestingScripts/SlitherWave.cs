﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlitherWave : MonoBehaviour
{
    private Vector3Int cellPosition;
    private Vector3 worldPosition;
    private int childIndex;
    private float amplitude;
    private float freq;
    public float hShift;
    private bool start;
    // Start is called before the first frame update
    void Start()
    {
        worldPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        amplitude = gameObject.GetComponentInParent<BuildGridPrototype>().amplitude;
        freq = gameObject.GetComponentInParent<BuildGridPrototype>().freq;
        if (start) {
            float wave = amplitude * Mathf.Sin((freq * Time.time) - hShift) + (2f * Mathf.Cos((2f * Time.time) - hShift));
            float cosWave = amplitude * Mathf.Cos((freq * Time.time) - hShift) + (2f * Mathf.Sin((2f * Time.time) - hShift));
            transform.position = new Vector3(cosWave + worldPosition.x, wave + worldPosition.y, transform.position.z);
        }
    }

    public void StartWave(bool start, Vector3Int cellPosition, int childIndex, float hShift)
    {
        this.childIndex = childIndex;
        this.cellPosition = cellPosition;
        this.hShift = hShift;
        this.start = start;
        gameObject.GetComponent<SpriteRenderer>().color = new Color32(50, 131, 178, 255);
    }
}

