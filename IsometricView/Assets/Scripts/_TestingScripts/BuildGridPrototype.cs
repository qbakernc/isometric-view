﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildGridPrototype : MonoBehaviour
{
    public GameObject tile;
    private int childCount;
    public float freq;
    public float amplitude;
    private float hShift;
    public float degrees = 0;
    public List<Vector3Int> address = new List<Vector3Int>();

    public int animationSelect = 0;
    // Start is called before the first frame update
    void Start()
    {
        int placeHolder = 0;
        if (animationSelect == 1) {
            for (int i = -20; i < 20; i++) {

                for (int j = -20; j < 20; j++) {
                    hShift = degrees * Mathf.Deg2Rad;
                    address.Add(new Vector3Int(j, i, 0));
                    GameObject myNewTile = Instantiate(tile, CellToWorld(j, i, 0), Quaternion.identity);
                    myNewTile.transform.parent = gameObject.transform;
                    myNewTile.transform.GetComponent<SineWave>().StartWave(true, new Vector3Int(j, i, 0), address.IndexOf(new Vector3Int(j, i, 0)), hShift);
                    degrees += 10f;
                }
                placeHolder += 5;
                degrees = placeHolder;

            }
        }
        else if (animationSelect == 2) {
            for (int i = -20; i < 20; i++) {
                for (int j = -20; j < 20; j++) {
                    address.Add(new Vector3Int(j, i, 0));
                    GameObject myNewTile = Instantiate(tile, CellToWorld(j, i, 0), Quaternion.identity);
                    myNewTile.transform.parent = gameObject.transform;
                    myNewTile.transform.GetComponent<SwissCheese>().StartWave(true, new Vector3Int(j, i, 0), address.IndexOf(new Vector3Int(j, i, 0)));
                }
            }
        }
        else if (animationSelect == 3) {
            for (int i = -20; i < 20; i++) {
                for (int j = -20; j < 20; j++) {
                    address.Add(new Vector3Int(j, i, 0));
                    GameObject myNewTile = Instantiate(tile, CellToWorld(j, i, 0), Quaternion.identity);
                    myNewTile.transform.parent = gameObject.transform;
                }
            }
            CenterWave(0, 0);
            CenterWave(-13, -12);
            CenterWave(11, 11);
            CenterWave(-11, 11);
            CenterWave(11, -11);
        } else if (animationSelect == 4) {
            degrees = 0;
            for (int i = -20; i < 20; i++) {

                for (int j = -20; j < 20; j++) {
                    hShift = degrees * Mathf.Deg2Rad;
                    address.Add(new Vector3Int(j, i, 0));
                    GameObject myNewTile = Instantiate(tile, CellToWorld(j, i, 0), Quaternion.identity);
                    myNewTile.transform.parent = gameObject.transform;
                    myNewTile.transform.GetComponent<SlitherWave>().StartWave(true, new Vector3Int(j, i, 0), address.IndexOf(new Vector3Int(j, i, 0)), hShift);
                }
                degrees += 10f;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            Destroy(gameObject);
        }
    }

    private void CenterWave(int x, int y)
    {
        Vector3Int northHead = new Vector3Int(0, 1, 0);
        Vector3Int southHead = new Vector3Int(0, -1, 0);
        Vector3Int eastHead = new Vector3Int(1, 0, 0);
        Vector3Int westHead = new Vector3Int(-1, 0, 0);
        gameObject.transform.GetChild(821).GetComponent<CenterSineWave>().StartWave(true, new Vector3Int(1, 0, 0), 821, 0 * Mathf.Rad2Deg);
        gameObject.transform.GetChild(address.IndexOf(new Vector3Int(x, y, 0))).GetComponent<CenterSineWave>().StartWave(true, new Vector3Int(1, 0, 0), 821, 0 * Mathf.Rad2Deg);
        int counter = 0;
        int cswShift = 10;
        while (counter <= 7) {
            northHead = new Vector3Int(x, counter + y, 0);
            southHead = new Vector3Int(x, y - counter, 0);
            eastHead = new Vector3Int(counter + x, y, 0);
            westHead = new Vector3Int(x - counter, y, 0);
            for (int i = 0; i < counter; i++) {
                gameObject.transform.GetChild(address.IndexOf(northHead)).GetComponent<CenterSineWave>().StartWave(true, northHead, address.IndexOf(northHead), cswShift * Mathf.Rad2Deg);
                gameObject.transform.GetChild(address.IndexOf(southHead)).GetComponent<CenterSineWave>().StartWave(true, southHead, address.IndexOf(southHead), cswShift * Mathf.Rad2Deg);
                gameObject.transform.GetChild(address.IndexOf(eastHead)).GetComponent<CenterSineWave>().StartWave(true, eastHead, address.IndexOf(eastHead), cswShift * Mathf.Rad2Deg);
                gameObject.transform.GetChild(address.IndexOf(westHead)).GetComponent<CenterSineWave>().StartWave(true, westHead, address.IndexOf(westHead), cswShift * Mathf.Rad2Deg);

                northHead = new Vector3Int(northHead.x + 1, northHead.y - 1, 0);
                southHead = new Vector3Int(southHead.x - 1, southHead.y + 1, 0);
                eastHead = new Vector3Int(eastHead.x - 1, eastHead.y - 1, 0);
                westHead = new Vector3Int(westHead.x + 1, westHead.y + 1, 0);
            }
            cswShift += 10;
            counter++;
        }
    }
    private Vector3 CellToWorld(float cX, float cY, float cZ)
    {
        // Checks to see if any elevation was added on the Z axis. Will this will affect the tiles location.
        cX = cX + (cZ * 0.25f);
        cY = cY + (cZ * 0.25f);

        // Coverts our tile from a cell location into a world location.
        float wX = (cX * 1) + (cY * -1);
        float wY = (cX * .5f) + (cY * .5f);

        return new Vector3(wX, wY, cZ);
    }
}
