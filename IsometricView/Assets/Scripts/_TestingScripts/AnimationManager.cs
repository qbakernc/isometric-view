﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager instance;
    public GameObject grid;
    private int clickCount = 0;
    // Start is called before the first frame update
    public void Awake()
    {
        if(instance == null){
            instance = this;
        }
        GameObject go = Instantiate(grid);
        go.transform.GetComponent<BuildGridPrototype>().animationSelect = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            clickCount++;
            if(clickCount == 5)
                clickCount = 1;
            GameObject go = Instantiate(grid);
            go.transform.GetComponent<BuildGridPrototype>().animationSelect = clickCount;
        }
    }
}
