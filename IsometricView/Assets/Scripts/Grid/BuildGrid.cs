﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildGrid : MonoBehaviour
{
    public GameObject tile;
    public GameObject hazardBlock;
    private int childCount;
    public List<Vector3Int> address = new List<Vector3Int>();

    // Start is called before the first frame update
    void Start()
    {

        for (int i = -5; i < 5; i++) {
            for (int j = -5; j < 5; j++) {
                address.Add(new Vector3Int(j, i, 0));
                GameObject myNewTile = Instantiate(tile, Utilities.CellToWorld(j, i, 0), Quaternion.identity);
                myNewTile.GetComponent<TileControl>().cellPosition = new Vector3Int(j, i, 0);
                myNewTile.GetComponent<TileControl>().z = 0;
                myNewTile.transform.parent = gameObject.transform;
            }
        }
        /*
        for(int i = -5; i < 5; i++) {
            if(i != 0){
            address.Add(new Vector3Int(4, i, 2));
            GameObject myNewTile = Instantiate(hazardBlock, Utilities.CellToWorld(4, i, 2), Quaternion.identity);
            myNewTile.GetComponent<MovingTile>().Setup(true, new Vector3Int(4, i, 2));
            myNewTile.transform.parent = gameObject.transform;
            }
        }
        */
    }
    // Update is called once per frame
    void Update()
    {

    }
}
