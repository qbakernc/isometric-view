﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    public static Vector3 CellToWorld(float cX, float cY, float z) 
    {
        float scale = 2;              // What the size of the tile has been scaled by.
        float a = 0.5f * scale;
        float b = 0.5f * scale;
        float c = 0.25f * scale;
        float d = 0.25f * scale;      
        float e = 0.5f / scale;       // Will be used to get our elevation
        z = z * scale;

        // Checks to see if any elevation was added on the Z axis. Will this will affect the tiles location.
        cX = cX + (z * e);
        cY = cY + (z * e);

        // Coverts our tile from a cell location into a world location.
        float wX = (cX * a) + (cY * -b);
        float wY = (cX * c) + (cY * d);

        return new Vector3(wX, wY, z);
    }

    public static Vector3Int WorldToCell(float wX, float wY, float z) 
    {
        float scale = 2;              // What the size of the tile has been scaled by.
        float a = 0.5f * scale;
        float b = 0.5f * scale;
        float c = 0.25f * scale;
        float d = 0.25f * scale;      
        float e = 0.5f / scale;       // Will be used to get our elevation
        z = z * scale;

        return new Vector3Int(0,0,0);
    }
}
